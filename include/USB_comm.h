#ifndef __USB_COMM_H__
#define __USB_COMM_H__

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/st_usbfs.h>
#include <libopencm3/usb/cdc.h>
#include <libopencm3/usb/usbd.h>
#include <libopencm3/usb/usbstd.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#define USB_VID 0x6666
#define USB_PID 0xCDC0

#define DATA_OUT_MAX_PACKET_LEN 64
#define DATA_IN_MAX_PACKET_LEN 64
#define TX_BUF_LEN 128  /* Must be at least DATA_OUT_MAX_PACKET_LEN. */
#define RX_BUF_LEN 128 
#define ARRAY_LEN(ARRAY) (sizeof(ARRAY) / sizeof(*(ARRAY)))


enum {
	DATA_END_OUT,
	DATA_END_IN,
	DATA_ENDS
};

enum {
	CDC_END_CONTROL,
	CDC_ENDS
};

enum {
	INT_CDC,
	INT_DATA,
	INTS
};

enum {
	USB_STRING_MANUFACTURER,
	USB_STRING_PRODUCT,
	USB_STRINGS
};

static const char *const usbStrings[USB_STRINGS] = {
	[USB_STRING_MANUFACTURER] = "FIUBA",
	[USB_STRING_PRODUCT] = "AOP Measurement Device",
};

static const struct {
	struct usb_cdc_header_descriptor headerDesc;
	struct usb_cdc_call_management_descriptor callManagementDesc;
	struct usb_cdc_acm_descriptor acmDesc;
	struct usb_cdc_union_descriptor unionDesc;
} __attribute__((packed)) usbCdcDesc = {
	.headerDesc = {
		.bFunctionLength = sizeof(usbCdcDesc.headerDesc),
		.bDescriptorType = CS_INTERFACE,
		.bDescriptorSubtype = USB_CDC_TYPE_HEADER,
		.bcdCDC = 0x0120,
	},
	.callManagementDesc = {
		.bFunctionLength = sizeof(usbCdcDesc.callManagementDesc),
		.bDescriptorType = CS_INTERFACE,
		.bDescriptorSubtype = USB_CDC_TYPE_CALL_MANAGEMENT,
		.bmCapabilities = 0,
		.bDataInterface = 0,
	},
	.acmDesc = {
		.bFunctionLength = sizeof(usbCdcDesc.acmDesc),
		.bDescriptorType = CS_INTERFACE,
		.bDescriptorSubtype = USB_CDC_TYPE_ACM,
		.bmCapabilities = 0,
	},
	.unionDesc = {
		.bFunctionLength = sizeof(usbCdcDesc.unionDesc),
		.bDescriptorType = CS_INTERFACE,
		.bDescriptorSubtype = USB_CDC_TYPE_UNION,
		.bControlInterface = INT_CDC,
		.bSubordinateInterface0 = INT_DATA,
	},
};
static const struct usb_endpoint_descriptor usbCdcEndDesc[CDC_ENDS] = {
	[CDC_END_CONTROL] = {
		.bLength = USB_DT_ENDPOINT_SIZE,
		.bDescriptorType = USB_DT_ENDPOINT,
		.bEndpointAddress = USB_ENDPOINT_ADDR_IN(3),
		.bmAttributes = USB_ENDPOINT_ATTR_INTERRUPT,
		.wMaxPacketSize = 1,
		.bInterval = 255,
	},
};
static const struct usb_interface_descriptor usbCdcIntDesc[] = {
	{
		.bLength = USB_DT_INTERFACE_SIZE,
		.bDescriptorType = USB_DT_INTERFACE,
		.bInterfaceNumber = INT_CDC,
		.bAlternateSetting = 0,
		.bNumEndpoints = ARRAY_LEN(usbCdcEndDesc),
		.bInterfaceClass = USB_CLASS_CDC,
		.bInterfaceSubClass = USB_CDC_SUBCLASS_ACM,
		.bInterfaceProtocol = USB_CDC_PROTOCOL_NONE,
		.iInterface = 0,
		.endpoint = usbCdcEndDesc,
		.extra = &usbCdcDesc,
		.extralen = sizeof(usbCdcDesc),
	},
};
static const struct usb_endpoint_descriptor usbDataEndDesc[DATA_ENDS] = {
	[DATA_END_OUT] = {
		.bLength = USB_DT_ENDPOINT_SIZE,
		.bDescriptorType = USB_DT_ENDPOINT,
		.bEndpointAddress = USB_ENDPOINT_ADDR_OUT(1),
		.bmAttributes = USB_ENDPOINT_ATTR_BULK,
		.wMaxPacketSize = DATA_OUT_MAX_PACKET_LEN,
		.bInterval = 0,
	},
	[DATA_END_IN] = {
		.bLength = USB_DT_ENDPOINT_SIZE,
		.bDescriptorType = USB_DT_ENDPOINT,
		.bEndpointAddress = USB_ENDPOINT_ADDR_IN(2),
		.bmAttributes = USB_ENDPOINT_ATTR_BULK,
		.wMaxPacketSize = DATA_IN_MAX_PACKET_LEN,
		.bInterval = 0,
	},
};
static const struct usb_interface_descriptor usbDataIntDesc[] = {
	{
		.bLength = USB_DT_INTERFACE_SIZE,
		.bDescriptorType = USB_DT_INTERFACE,
		.bInterfaceNumber = INT_DATA,
		.bAlternateSetting = 0,
		.bNumEndpoints = ARRAY_LEN(usbDataEndDesc),
		.bInterfaceClass = USB_CLASS_DATA,
		.bInterfaceSubClass = 0,
		.bInterfaceProtocol = 0,
		.iInterface = 0,
		.endpoint = usbDataEndDesc,
		.extra = NULL,
		.extralen = 0,
	},
};
static const struct usb_interface usbInt[INTS] = {
	[INT_CDC] = {
		.num_altsetting = ARRAY_LEN(usbCdcIntDesc),
		.altsetting = usbCdcIntDesc,
	},
	[INT_DATA] = {
		.num_altsetting = ARRAY_LEN(usbDataIntDesc),
		.altsetting = usbDataIntDesc,
	},
};
static const struct usb_config_descriptor usbConfigDesc[] = {
	{
		.bLength = USB_DT_CONFIGURATION_SIZE,
		.bDescriptorType = USB_DT_CONFIGURATION,
		.wTotalLength = 0,
		.bNumInterfaces = ARRAY_LEN(usbInt),
		.bConfigurationValue = 1,
		.iConfiguration = 0,
		.bmAttributes = USB_CONFIG_ATTR_DEFAULT,
		.bMaxPower = 50 / 2,
		.interface = usbInt,
	},
};
static const struct usb_device_descriptor usbDevDesc = {
	.bLength = USB_DT_DEVICE_SIZE,
	.bDescriptorType = USB_DT_DEVICE,
	.bcdUSB = 0x0200,
	.bDeviceClass = 0,
	.bDeviceSubClass = 0,
	.bDeviceProtocol = 0,
	.bMaxPacketSize0 = 64,
	.idVendor = USB_VID,
	.idProduct = USB_PID,
	.bcdDevice = 0x0000,
	.iManufacturer = USB_STRING_MANUFACTURER + 1,
	.iProduct = USB_STRING_PRODUCT + 1,
	.iSerialNumber = 0,
	.bNumConfigurations = ARRAY_LEN(usbConfigDesc),
};





void UsbInitSerial(void);
void usbSetConfig (usbd_device *usbdDeva, uint16_t wValue);
void TxData(usbd_device *usbdDev,uint8_t endpointAddress);
void RxData(usbd_device *usbdDev,uint8_t endpointAddress);
bool UsbWritable(void);

extern uint_least8_t TxBuf[TX_BUF_LEN];
extern uint_fast16_t TxBufFill;
extern uint_least8_t RxBuf[RX_BUF_LEN];
extern uint_fast16_t RxBufFill;


#endif