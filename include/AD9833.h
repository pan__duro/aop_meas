#ifndef __AD9833_H__
#define __AD9833_H__

#include <stdint.h>
#include <stdio.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/spi.h>


#define D15 (1<<15)
#define D14 (1<<14)
#define D13 (1<<13)
#define B28 (1<<13)
#define HLB (1<<12)
#define FSELECT (1<<11)
#define PSELECT (1<<10)
#define RESET (1<<8)
#define SLEEP1 (1<<7)
#define SLEEP12 (1<<6)
#define OPBITEN (1<<5)
#define DIV2 (1<<3)
#define MODE (1<<1)
#define AD9833_CONFIG (B28)

#define fq1maskand 0b0011111111111111
#define fq1maskor 0b0100000000000000
#define ph1maskand  0b1101111111111111
#define ph1markor 0b1100000000000000
#define ftmask 0b0011111111111111

#ifndef USE_16BIT_TRANSFERS
#define USE_16BIT_TRANSFERS 1
#endif

void spi_setup(void);
void AD9833_write(u_int16_t data);
void AD9833_set(uint32_t freq, uint32_t phase,uint8_t reset) ;
void AD9833_Init(void);
void AD9833_set_dc(u_int32_t val);
uint64_t AD9833_freq_calc(uint64_t freq, uint64_t clk);
#endif