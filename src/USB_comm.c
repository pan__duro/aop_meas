/* cdc-echo.c - Simple USB CDC-ACM demo for libopencm3 on STM32F1 */

/* This file is in the public domain. */

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/st_usbfs.h>
#include <libopencm3/usb/cdc.h>
#include <libopencm3/usb/usbd.h>
#include <libopencm3/usb/usbstd.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <libopencm3/stm32/gpio.h>

#include "USB_comm.h"

static bool usbdWritable = false;
static bool startIn = false;


uint_least8_t TxBuf[TX_BUF_LEN];
uint_fast16_t TxBufFill = 0;
uint_least8_t RxBuf[RX_BUF_LEN];
uint_fast16_t RxBufFill = 0;


void usbDataIn (usbd_device *usbdDeva, uint8_t endpointAddress);

bool UsbWritable(void){
	return usbdWritable;
}

// static void usbDataOut (usbd_device *usbdDeva, uint8_t endpointAddress) {
// 	uint_fast16_t packetLen = usbd_ep_read_packet(usbdDeva, endpointAddress, echoBuf + echoBufFill, sizeof(echoBuf) - echoBufFill);
// 	if (packetLen) {
// 		if (!echoBufFill)
// 			startIn = true;
// 		echoBufFill += packetLen;

// 		if (echoBufFill > sizeof(echoBuf) - usbDataEndDesc[DATA_END_OUT].wMaxPacketSize)
// 			usbd_ep_nak_set(usbdDeva, endpointAddress, 1);
// 	}
// }

//  void usbDataIn (usbd_device *usbdDeva, uint8_t endpointAddress) {
// 	if (!echoBufFill)
// 		return;

// 	uint_fast16_t packetLen = echoBufFill;
// 	if (packetLen > usbDataEndDesc[DATA_END_IN].wMaxPacketSize)
// 		packetLen = usbDataEndDesc[DATA_END_IN].wMaxPacketSize;

// 	packetLen = usbd_ep_write_packet(usbdDeva, endpointAddress, echoBuf, packetLen);

// 	if (packetLen) {
// 		echoBufFill -= packetLen;
// 		if (echoBufFill)
// 			memmove(echoBuf, echoBuf + packetLen, echoBufFill);

// 		if (echoBufFill <= sizeof(echoBuf) - usbDataEndDesc[DATA_END_OUT].wMaxPacketSize)
// 			usbd_ep_nak_set(usbdDeva, usbDataEndDesc[DATA_END_OUT].bEndpointAddress, 0);
// 	}
// }

static void setupEp (usbd_device *usbdDeva, const struct usb_endpoint_descriptor endDesc[restrict static 1], usbd_endpoint_callback callback) {
	usbd_ep_setup(usbdDeva, endDesc->bEndpointAddress, endDesc->bmAttributes, endDesc->wMaxPacketSize, callback);
}

 void usbSetConfig (usbd_device *usbdDeva, uint16_t wValue) {
	(void)wValue;

	setupEp(usbdDeva, &usbDataEndDesc[DATA_END_OUT], TxData);
	setupEp(usbdDeva, &usbDataEndDesc[DATA_END_IN], RxData);
	setupEp(usbdDeva, &usbCdcEndDesc[CDC_END_CONTROL], NULL);

	usbdWritable = true;
}

void UsbInitSerial (void) {
	//rcc_clock_setup_in_hse_8mhz_out_72mhz();

	uint_least8_t usbControlBuffer[usbDevDesc.bMaxPacketSize0];
	usbd_device *usbdDev;
	usbdDev = usbd_init(&st_usbfs_v1_usb_driver, &usbDevDesc, usbConfigDesc, (const char **)usbStrings, ARRAY_LEN(usbStrings), usbControlBuffer, sizeof(usbControlBuffer));
	usbd_register_set_config_callback(usbdDev, usbSetConfig);

	//for (;;) {
		usbd_poll(usbdDev);
		if (startIn && usbdWritable) {
			usbDataIn(usbdDev, usbDataEndDesc[DATA_END_IN].bEndpointAddress);
			startIn = false;
		}
	//}
}


void TxData(usbd_device *usbdDeva,uint8_t endpointAddress)
{//transmite el buffer
	if (!TxBufFill)// si está vacio retorna
		return;

	uint_fast16_t packetLen = TxBufFill;
	if (packetLen > usbDataEndDesc[DATA_END_IN].wMaxPacketSize)
		packetLen = usbDataEndDesc[DATA_END_IN].wMaxPacketSize;

	packetLen = usbd_ep_write_packet(usbdDeva, endpointAddress, TxBuf, packetLen);

	if (packetLen) {
		TxBufFill -= packetLen;
		if (TxBufFill)
			memmove(TxBuf, TxBuf + packetLen, TxBufFill);

		if (TxBufFill <= sizeof(TxBuf) - usbDataEndDesc[DATA_END_OUT].wMaxPacketSize)
			usbd_ep_nak_set(usbdDeva, usbDataEndDesc[DATA_END_OUT].bEndpointAddress, 0);
	}
}

void RxData(usbd_device *usbdDeva,uint8_t endpointAddress)
{
	uint_fast16_t packetLen = usbd_ep_read_packet(usbdDeva, endpointAddress, RxBuf + RxBufFill, sizeof(RxBuf) - RxBufFill);
	if (packetLen) {
		RxBufFill += packetLen;
		if (RxBufFill > sizeof(RxBuf) - usbDataEndDesc[DATA_END_OUT].wMaxPacketSize)// si no hay lugar pone NACK para que reciba el S.O.
			usbd_ep_nak_set(usbdDeva, endpointAddress, 1);
	}
}

