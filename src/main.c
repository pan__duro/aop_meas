/* cdc-echo.c - Simple USB CDC-ACM demo for libopencm3 on STM32F1 */

/* This file is in the public domain. */

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/st_usbfs.h>
#include <libopencm3/usb/cdc.h>
#include <libopencm3/usb/usbd.h>
#include <libopencm3/usb/usbstd.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>

#include <libopencm3/cm3/nvic.h>
#include <libopencm3/stm32/spi.h>
#include <errno.h>

#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/stm32/adc.h>
#include <serial.h>

#include "AD9833.h"
#include "USB_comm.h"

#define COEFF_0100  400224125
#define COEFF_1000   40381198
#define COEFF_10K     4384811

int _write(int file, char *ptr, int len);
usbd_device *usbdDev;
volatile long millis;
volatile long adc_coeff;


void tim2_isr(void) //interrupcion que salta 1000 veces/seg. usar para samplear !! .
{
    timer_clear_flag(TIM2, TIM_SR_UIF);
    millis++;
    //gpio_toggle(GPIOC, GPIO13);
    // ...
}

int main(void)
{
    rcc_clock_setup_in_hse_8mhz_out_72mhz();

    /* Configuración del ADC */
    // Se configura el prescaler del ADC:
    // 72Mhz/6 = 12Mhz (14Mhz es el máximo permitido)
    rcc_set_adcpre(RCC_CFGR_ADCPRE_PCLK2_DIV6);
    // Se habilita el Clock del periférico
    rcc_periph_clock_enable(RCC_ADC1);
    // Se prende el ADC1
    adc_power_on(ADC1);
    // 2 ADC Cycles delay..
    for (uint16_t i = 0; i < 720 ; i++) __asm__("nop");
    // Se hace una calibración como recomienda la hoja de datos:
    adc_reset_calibration(ADC1);
    adc_calibrate(ADC1);

    /* Configuración E/S */
    // Se utilizará el CH04 del ADC, que es PA4
    rcc_periph_clock_enable(RCC_GPIOA);
    gpio_set_mode(GPIOA, GPIO_MODE_INPUT, GPIO_CNF_INPUT_ANALOG, GPIO4);
    /* Enable GPIOA, GPIOB, GPIOC clock. */
    rcc_periph_clock_enable(RCC_GPIOA);
    rcc_periph_clock_enable(RCC_GPIOB);
    rcc_periph_clock_enable(RCC_GPIOC);
    /* Enable clocks for GPIO port A (for GPIO_USART2_TX) and USART2. */
    rcc_periph_clock_enable(RCC_GPIOA);
    rcc_periph_clock_enable(RCC_AFIO);
    rcc_periph_clock_enable(RCC_USART2);
    /* Enable SPI1 Periph and gpio clocks */
    // Para poder usar los pines PB3 y PB4 se inhabilita el JTAG.
    rcc_periph_clock_enable(RCC_AFIO);
    //asumo que SWJ_CFG = 000
    AFIO_MAPR |= (1 << 25); // plancho un 1 ->  SWJ_CFG = 010 ,JTAG-DP Disabled and SW-DP Enabled

    /* Enable SPI1 Periph and gpio clocks */
    rcc_periph_clock_enable(RCC_SPI1);

    rcc_periph_clock_enable(RCC_TIM2); /* configuro el timer de prosito general*/
    timer_set_mode(
        TIM2,                             // Timer2
        TIM_CR1_CKD_CK_INT,               // El clk interno 72Mhz
        TIM_CR1_CMS_EDGE,                 // Alineado al flanco (PWM)
        TIM_CR1_DIR_UP);                  // Cuenta ascendente  __/⁻⁻
    timer_set_prescaler(TIM2, 3600 - 1); // 72Mhz / 3.600 => 20.000 Hz
    timer_set_period(TIM2, 20 - 1);      // 20.000/20 => 0.001s
    timer_enable_irq(TIM2, TIM_DIER_UIE); // habilitar la interrupcion cada vez que se recarga
    timer_enable_counter(TIM2);           // habilitar la cuenta del timer
    nvic_enable_irq(NVIC_TIM2_IRQ); // habilitar la IRQ en el NVIC del TIM2

    gpio_set_mode(GPIOC, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO13);
    spi_setup();
    int16_t k = 0;
    
    //inicializa el USB en modo CDC
    uint_least8_t usbControlBuffer[usbDevDesc.bMaxPacketSize0];
	usbdDev = usbd_init(&st_usbfs_v1_usb_driver, &usbDevDesc, usbConfigDesc, (const char **)usbStrings, ARRAY_LEN(usbStrings), usbControlBuffer, sizeof(usbControlBuffer));
	usbd_register_set_config_callback(usbdDev, usbSetConfig); 
    

    /*    
    AD9833_set(1000, 0, 0);//set dc
    AD9833_set_dc(512);//set ac */
    while (1)
    {   
        usbd_poll(usbdDev);
        TxData(usbdDev, usbDataEndDesc[DATA_END_IN].bEndpointAddress);
        
        //strcpy((char *)TxBuf,"hola Mundeee\n ");
        //TxBufFill=11;
        //if(UsbWritable())
        
   		
        
        AD9833_set(k, 0, 0);
        k += 1;
        while (millis>1000)
        {
            gpio_toggle(GPIOC, GPIO13);
            printf("hola mundo!!! %ld \n",millis);
            millis=500;
        }
        
    }
}

int _write(int file, char *ptr, int len)
{
    if(UsbWritable() &&  TxBufFill+len <=TX_BUF_LEN ){
	int i;
	if (file == STDOUT_FILENO || file == STDERR_FILENO) {
		for (i = 0; i < len; i++) {
            TxBuf[TxBufFill+i]=ptr[i];
            }
            TxBufFill+=len;
            TxData(usbdDev, usbDataEndDesc[DATA_END_IN].bEndpointAddress);
		return i;
	}
    }
	errno = EIO;
	return -1;
}