#include <math.h>
#include <AD9833.h>

void spi_setup(void)
{
	/* Configure GPIOs: SS=PA4, SCK=PA5, MISO=PA6 and MOSI=PA7
	 * For now ignore the SS pin so we can use it to time the ISRs
	 */
	gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ,
				  GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, /*GPIO4 |*/
				  GPIO5 |
					  GPIO7);

	gpio_set_mode(GPIOA, GPIO_MODE_INPUT, GPIO_CNF_INPUT_FLOAT,
				  GPIO6);

	/* Configuro el pin como salida para usar el SS por soft.*/
	gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO4);

	/* Reset SPI, SPI_CR1 register cleared, SPI is disabled */
	spi_reset(SPI1);

	/* Explicitly disable I2S in favour of SPI operation */
	SPI1_I2SCFGR = 0;

	/* Set up SPI in Master mode with:
	 * Clock baud rate: 1/256 of peripheral clock frequency
	 * Clock polarity: Idle High
	 * Clock phase: Data valid on 2nd clock pulse
	 * Data frame format: 8-bit or 16-bit
	 * Frame format: MSB First
	 */
#if USE_16BIT_TRANSFERS
	spi_init_master(SPI1, SPI_CR1_BAUDRATE_FPCLK_DIV_256, SPI_CR1_CPOL_CLK_TO_1_WHEN_IDLE,
					SPI_CR1_CPHA_CLK_TRANSITION_1, SPI_CR1_DFF_16BIT, SPI_CR1_MSBFIRST);
#else
	spi_init_master(SPI1, SPI_CR1_BAUDRATE_FPCLK_DIV_64, SPI_CR1_CPOL_CLK_TO_1_WHEN_IDLE,
					SPI_CR1_CPHA_CLK_TRANSITION_2, SPI_CR1_DFF_8BIT, SPI_CR1_MSBFIRST);
#endif

	/*
	 * Set NSS management to software.
	 *
	 * Note:
	 * Setting nss high is very important, even if we are controlling the GPIO
	 * ourselves this bit needs to be at least set to 1, otherwise the spi
	 * peripheral will not send any data out.
	 */

	//spi_enable_ss_output(SPI1);
	//SPI1_CR2 |= SPI_CR2_SSOE; //habilito el pin como salida para manejar el AD
	spi_enable_software_slave_management(SPI1);
	spi_set_nss_high(SPI1);

	/* Enable SPI1 periph. */
	spi_enable(SPI1);
}

uint64_t AD9833_freq_calc(uint64_t freq, uint64_t clk)
{
	static uint64_t ret;
	static uint64_t retf;

	retf = (freq * (2 << 27)) / clk;
	ret = retf;
	return ret;
}

void AD9833_set_dc(u_int32_t phase)
{
	/* TODO: Solucionar un pequeño offset entre el valor de fase seteado y la salida del DAC */
	phase += 3072; // me situo en la parte de pendiente positiva
	phase &= ph1maskand;
	phase |= ph1markor;

	AD9833_write(fq1maskor); //plancho la frecuencia en 0
	AD9833_write(fq1maskor);
	AD9833_write(phase);
	AD9833_write(AD9833_CONFIG | MODE); // Seteo modo triangular
}

void AD9833_write(uint16_t data)
{
	gpio_clear(GPIOA, GPIO4);
	spi_send(SPI1, data); // 16-bit write
}
void AD9833_Init(void)
{
	uint16_t data;
	gpio_set(GPIOA, GPIO4);		  // pongo en 1 el pin SS para terminar la comunicación, si es que existia una
	for (int i = 0; i < 800; i++) /* Wait a bit. */
		__asm__("nop");
	gpio_clear(GPIOA, GPIO4);
	for (int i = 0; i < 800; i++) /* Wait a bit. */
		__asm__("nop");
	data = AD9833_CONFIG | RESET; // B28 + RESET
	AD9833_write(data);
}
void AD9833_set(uint32_t freq, uint32_t phase, uint8_t reset)
{
	static uint16_t fqlsb;
	static uint16_t fqmsb;

	static uint64_t FreqReg;
	static uint32_t PhReg;
	const uint64_t fmclk = 25000000;

	if (reset)
		AD9833_Init();

	//FreqReg = (freq << 28) / fmclk;
	FreqReg = AD9833_freq_calc(freq, fmclk);
	fqlsb = FreqReg & ftmask;
	fqmsb = (FreqReg >> 14) & ftmask;

	fqlsb = fq1maskand & fqlsb;
	fqlsb = fq1maskor | fqlsb;
	fqmsb = fq1maskand & fqmsb;
	fqmsb = fq1maskor | fqmsb;

	PhReg = phase;
	//PhReg &= twmask;

	PhReg &= ph1maskand;
	PhReg |= ph1markor;

	AD9833_write(fqlsb);
	AD9833_write(fqmsb);
	AD9833_write(PhReg);
	//	if (reset)
	AD9833_write(AD9833_CONFIG); // Configura la salida como senoidal
}
